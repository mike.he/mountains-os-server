## awesome-laravel-api

#### Docker 环境搭建

- 安装docker-compose
> 依赖包: py-pip, python-dev, libffi-dev, openssl-dev, gcc, libc-dev, and make.
```bash
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```
---
- 启动开发环境
```bash
$ docker-compose up -d
``` 
---
- 卸载docker-compose
```bash
$ sudo rm /usr/local/bin/docker-compose
```
