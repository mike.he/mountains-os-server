<?php

namespace App\Routes;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@showHelloWorldAction');

Route::namespace('AsyncUseCase')->group(function () {
    Route::get('/async', 'AsyncController@getAsyncAction');
});

Route::namespace('DoctrineUseCase')->group(function () {
    Route::post('/scientist', 'DoctrineController@insertEntitiesAction');
    Route::get('/scientists', 'DoctrineController@getEntitiesAction');
});
