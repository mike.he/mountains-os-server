<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PHPSocketIO\SocketIO;
use Workerman\Worker;

class SocketIOServer extends Command
{
    protected $signature = 'socket-io:server {action} {--option=}';

    protected $description = 'SocketIO Server';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $action = $this->argument('action');
        $option = $this->option('option');
        global $argv;

        $argv[0] = 'gateway:server';
        $argv[1] = $action;
        $argv[2] = $option;

        $io = new SocketIO(2346);

        $io->on('connection', function($socket) use($io){
            echo 'connected!';
            $socket->on('chat-message', function($msg) use($io){
                var_dump($msg);
                $io->emit('news', $msg);
            });

            $socket->on('read-file', function($msg) use($io){
                $content = app('ide_service')->read($msg['path']);
                $io->emit('read-file', $content);
            });

            $socket->on('edit-file', function($msg) use($io){
                app('ide_service')->edit($msg['path'], $msg['code_diff']);
                var_dump($msg);
            });

            $socket->on('write-file', function($msg) use($io){
                app('ide_service')->write($msg['path'], $msg['code']);
                var_dump($msg);
            });

            $socket->on('disconnect', function($msg) use($io) {
                echo 'disconnection!' . PHP_EOL;
            });
        });

        Worker::runAll();
    }
}
