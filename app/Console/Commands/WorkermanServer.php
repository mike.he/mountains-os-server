<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Workerman\Worker;
use Workerman\Lib\Timer;

class WorkermanServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workerman:server {action} {--option=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Workerman Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        $option = $this->option('option');
        global $argv;

        $argv[0] = 'workerman:server';

        $argv[1] = $action;

        $argv[2] = $option;
        
        // worker
        $worker = new Worker();
        $worker->name = 'FileMonitor';
        $worker->reloadable = false;
        $last_mtime = time();
        $worker->onWorkerStart = function()
        {
            $monitor_dir = realpath(__DIR__ . '/../../../app');

            // watch files only in daemon mode
            if(!Worker::$daemonize)
            {
                // chek mtime of files per second
                Timer::add(1, [$this, 'check_files_change'], array($monitor_dir));
            }
        };

        $worker = new Worker('async://127.0.0.1:2017');

        $worker->count = 16;

        $worker->name = 'PromiseServer';

        $worker->onMessage = function ($connection, $data) {
            if (empty($data['class']) || empty($data['method']) || !isset($data['params'])) {
                return $connection->send([
                    'code' => 400,
                    'msg' => 'bad request',
                    'data' => null
                ]);
            }

            $class = $data['class'];
            $method = $data['method'];
            $params = $data['params'];

            // check class exist
            if(!class_exists($class))
                return $connection->send([
                    'code' => 404,
                    'msg' => "class $class not found",
                    'data' => null
                ]);

            // check method exist
            if (!method_exists($class, $method)) {
                return $connection->send([
                    'code' => 404,
                    'msg' => "method $method not found",
                    'data' => null
                ]);
            }

            try {
                $ret = call_user_func([$class, $method], ...$params);

                return $connection->send($ret);
            } catch(Exception $e) {
                $code = $e->getCode() ?? 500;

                return $connection->send([
                    'code' => $code,
                    'msg' => $e->getMessage(),
                    'data' => $e
                ]);
            }
        };

        Worker::runAll();
    }

    // check files func
    function check_files_change($monitor_dir)
    {
        global $last_mtime;
        // recursive traversal directory
        $dir_iterator = new \RecursiveDirectoryIterator($monitor_dir);
        $iterator = new \RecursiveIteratorIterator($dir_iterator);
        foreach ($iterator as $file)
        {
            $exclude_dir = '/home/vagrant/workspace/awesome-laravel-api/app/Console';

            if (strpos(pathinfo($file, PATHINFO_DIRNAME), $exclude_dir) !== false) {
                continue;
            }

            // only check php files
            if(pathinfo($file, PATHINFO_EXTENSION) != 'php')
            {
                continue;
            }
            
            // check mtime
            if($last_mtime < $file->getMTime())
            {
                echo "\033[32m[ Updated ]:\033[0m \033[34m[ $file ]\033[0m \n";
                // send SIGUSR1 signal to master process for reload
                posix_kill(posix_getppid(), SIGUSR1);
                $last_mtime = $file->getMTime();
                break;
            }
        }
    }
}
