<?php

namespace App\Entities\Forms;

class GatewayForm
{
    private $channel;

    private $method;

    private $params;

    public function getChannel()
    {
        return $this->channel;
    }

    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getParams()
    {
        return $this->params ?? [];
    }

    public function setParams($params)
    {
        $this->params = $params;
    }
}