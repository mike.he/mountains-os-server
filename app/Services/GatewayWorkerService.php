<?php

namespace App\Services;

use GatewayWorker\Lib\Gateway;

class GatewayWorkerService
{
    public function clientRegister($client_id, $uid) {
        Gateway::bindUid($client_id, $uid);
    }

    public function msgSend(
        $client_id,
        $uid,
        $msg
    ) {
        $msgJson = json_encode([
            'from' => Gateway::getUidByClientId($client_id),
            'body' => $msg,
        ]);
        Gateway::sendToUid($uid, $msgJson);
    }

    public function textMsgSend(
        $client_id,
        $msg
    ) {
        Gateway::sendToClient($client_id, $msg);
    }

    public function xtermRenderSend(
        $client_id,
        $uid,
        $msg
    ) {
        Gateway::sendToUid($uid, $msg);
    }
}