<?php

namespace App\Services;

use Illuminate\Support\Facades\File;
use Ramsey\Uuid\Uuid;

class IDEService
{
    const FILE_PATH_PREFIX = '/data/file-store';

    public function write(
        $filePath,
        $txt
    ) {
        $filePath = self::FILE_PATH_PREFIX . $filePath;

        $file = fopen($filePath, 'w');

        if (substr($txt, -1, 1) != PHP_EOL) {
            $txt .= PHP_EOL;
        }

        fwrite($file, $txt);

        fclose($file);
    }

    public function edit(
        $filePath,
        $diffTxt
    ) {
        $filePath = self::FILE_PATH_PREFIX . $filePath;
        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, time())->toString();

        $path = "storage/framework/cache/data/$uuid.patch";
        $file = fopen($path, 'w');

        $prefix = <<<CODE
diff --git a$filePath b$filePath
--- a$filePath
+++ b$filePath
CODE;

        $diffTxt = $prefix . PHP_EOL . $diffTxt . PHP_EOL;

        fwrite($file, $diffTxt);

        fclose($file);

        app('cli_service')->runCmd("cd ../../ && git apply /data/mountains-os-server/$path");

        unlink($path);
    }

    public function read(
        $filePath
    ) {
        $filePath = self::FILE_PATH_PREFIX . $filePath;

        $isExist = File::exists($filePath);

        if (!$isExist) return;

        $content = File::get($filePath);

        return $content;
    }
}
