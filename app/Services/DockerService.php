<?php

namespace App\Services;

class DockerService
{
    public function run(
        $client_id,
        $cmd
    ) {
        $cmd = "docker ps --format '{{json .}}'";

        $descriptorspec = array(
            0=>array("pipe", "r"),
            1=>array("pipe", "w"),
            2=>array("pipe", "w")
        );

        $process = proc_open($cmd, $descriptorspec, $pipes);

        if (!is_resource($process)) {
            return $this->handleResponse(1, "", "");
        }

        $str[] = stream_get_contents($pipes[0]);
        $str[] = stream_get_contents($pipes[1]);
        $str[] = stream_get_contents($pipes[2]);
        fwrite($pipes[0], "ls");

        fclose($pipes[0]);
        fclose($pipes[1]);
        fclose($pipes[2]);

        proc_close($process);

        $stdout = explode("\n", $str[1]);

        foreach($stdout as $key => &$item) {
            if (empty($item)) unset($stdout[$key]);

            $item = json_decode($item, true);
        }

        $data = json_encode($stdout, JSON_UNESCAPED_UNICODE);

        app('gateway_service')->textMsgSend($client_id, $data);

        $msgPack = [
            'channel'
        ];

        var_dump(json_encode($stdout, JSON_UNESCAPED_UNICODE));

        return json_encode($stdout, JSON_UNESCAPED_UNICODE);
    }

    private function handleResponse(
        $errorCode,
        $errorMessage,
        $data
    ) {
        return [
            'error_code' => $errorCode,
            'error_message' => $errorMessage,
            'data' => $data,
        ];
    }
}