<?php

namespace App\Services;

class CliService
{
    public function runCmd(
        $cmd
    ) {
        $descriptorspec = [
            ['pipe', 'r'],
            ['pipe', 'w'],
            ['pipe', 'w'],
        ];

        $process = proc_open($cmd, $descriptorspec, $pipes);

        $str[] = stream_get_contents($pipes[0]);
        $str[] = stream_get_contents($pipes[1]);
        $str[] = stream_get_contents($pipes[2]);

        fclose($pipes[0]);
        fclose($pipes[1]);
        fclose($pipes[2]);

        proc_close($process);

        $stdout = explode("\n", $str[1]);

        foreach($stdout as $key => &$item) {
            if (empty($item)) unset($stdout[$key]);
        }

        return $stdout;
    }
}