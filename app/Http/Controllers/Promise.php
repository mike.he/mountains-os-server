<?php

namespace App\Promise;

class Promise
{
    /**
     * @var resource
     */
    protected $connection = null;

    /**
     * @param String $method
     * @param Array $arguments
     * @return Boolean
     */
    public function __construct($service, $method, ...$arguments)
    {
        $this->openConnection();

        $bin_data = serialize([
            'class' => $service,
            'method' => $method,
            'params' => $arguments,
        ]) . "\n";

        if(fwrite($this->connection, $bin_data) !== strlen($bin_data))
        {
            throw new \Exception('Can not send data');
        }
        return true;
    }

    /**
     * @return String
     * @throws Exception
     */
    public function await()
    {
        $ret = fgets($this->connection);
        $this->closeConnection();
        if (!$ret) {
            throw new \Exception("await data is empty");
        }
        return unserialize($ret);
    }

    
    /**
     * @return Void
     * @throws Exception
     */
    protected function openConnection()
    {
        $address = 'tcp://127.0.0.1:2017';
        $this->connection = stream_socket_client($address, $err_no, $err_msg);
        
        stream_set_blocking($this->connection, 1);
        stream_set_timeout($this->connection, 60);
    }

    /**
     * @return Void
     */
    protected function closeConnection()
    {
        fclose($this->connection);
        $this->connection = null;
    }
}