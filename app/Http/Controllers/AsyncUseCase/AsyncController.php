<?php

namespace App\Http\Controllers\AsyncUseCase;

use Illuminate\Routing\Controller as BaseController;
use App\Promise\Promise;
use Illuminate\Http\Response;

class AsyncController extends BaseController
{
    public function getAsyncAction() {
        $promise = new Promise(
            AsyncController::class,
            'testAsync',
            2
        );
        $promise2 = new Promise(
            AsyncController::class,
            'testAsync',
            2
        );
        $ret = $promise->await();
        $ret2 = $promise2->await();

        return new Response([
            $ret,
            $ret2
        ]);
    }

    public static function testAsync()
    {
        sleep(2);
        return 22;
    }
}