<?php

namespace App\Http\Controllers\DoctrineUseCase;

use App\Entities\Scientist;
use App\Entities\Theory;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use LaravelDoctrine\ORM\Facades\EntityManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use JMS\Serializer\SerializerBuilder;

class DoctrineController extends BaseController
{
    /**
     * @Method('POST')
     * @Route('/scientist')
     *
     * @return Response
     */
    public function insertEntitiesAction() {
        EntityManager::beginTransaction();

        $scientist = new Scientist();
        $scientist->setName('Mike');
        $scientist->setPhone('123');

        $theory = new Theory();
        $theory->setTitle('Theory');

        $scientist->addTheory($theory);

        EntityManager::persist($scientist);
        EntityManager::flush();

        EntityManager::commit();

        $id = $scientist->getId();

        return new Response([
            'id' => $id
        ]);
    }

    /**
     * @Method('GET')
     * @Route('/scientists')
     *
     * @return Response
     */
    public function getEntitiesAction() {
        $scientist = EntityManager::getRepository(Scientist::class)
            ->getAll();

        $response = app('serializer')->serialize($scientist, 'json');

        return new Response($response);
    }
}
